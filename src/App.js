import './App.css';
import Border from './components/Border/Border';
import Color from './components/Color/Color';
import FontWeight from './components/FontWeight/FontWeight';


function App() {
  return (
    <div className="App">
      <Border title="Primeiro teste"><Color color='green'>Primeiro Elemento</Color></Border>
      <Border title="Segundo teste"><FontWeight fontWeight='900'>Segundo Componente</FontWeight></Border>
    </div>
  );
}

export default App;
